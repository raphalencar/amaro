package br.com.rba.amaro.commons;

public class ProductExtras {
    public static final String TAG_PRODUCTS = "products";
    public static final String TAG_PRODUCT = "product";
    public static final String TAG_NAME = "name";
    public static final String TAG_ON_SALE = "on_sale";
    public static final String TAG_REGULAR_PRICE = "regular_price";
    public static final String TAG_ACTUAL_PRICE = "actual_price";
    public static final String TAG_IMAGE = "image";
    public static final String TAG_SIZES = "sizes";
    public static final String TAG_SIZE = "size";
    public static final String TAG_AVAILABLE = "available";
}
