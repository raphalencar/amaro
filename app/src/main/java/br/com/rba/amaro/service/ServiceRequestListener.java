package br.com.rba.amaro.service;

import org.json.JSONObject;

public interface ServiceRequestListener {

    void onResponse(JSONObject response);
    void onError(String error);
}
