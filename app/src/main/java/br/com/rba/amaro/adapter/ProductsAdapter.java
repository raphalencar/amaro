package br.com.rba.amaro.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.rba.amaro.R;
import br.com.rba.amaro.model.Product;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    private Context mContext;
    private List<Product> productList;

    public class ProductsViewHolder extends RecyclerView.ViewHolder {

        private ImageView thumbnail;
        public TextView name;
        private TextView price;
        private TextView salePrice;

        private ProductsViewHolder(View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            name = itemView.findViewById(R.id.product_name);
            price = itemView.findViewById(R.id.product_price);
            salePrice = itemView.findViewById(R.id.product_sale_price);
        }
    }

    public ProductsAdapter(Context mContext, List<Product> productList) {
        this.mContext = mContext;
        this.productList = productList;
    }

    @Override
    public ProductsAdapter.ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_card, parent, false);

        return new ProductsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ProductsViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.name.setText(product.getName());

        if ((holder.price.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0) {
            holder.price.setPaintFlags(holder.price.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (product.isOnSale()) {
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.price.setText(product.getRegularPrice());
            holder.salePrice.setText(product.getActualPrice());
            holder.salePrice.setVisibility(View.VISIBLE);
        } else {
            holder.salePrice.setVisibility(View.GONE);
            holder.price.setText(product.getRegularPrice());
        }

        Glide.with(mContext).load(product.getImage()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void update(List<Product> list) {
        this.productList = list;
    }
}
