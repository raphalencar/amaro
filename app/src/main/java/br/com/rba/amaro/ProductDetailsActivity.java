package br.com.rba.amaro;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.Iterator;

import br.com.rba.amaro.commons.ProductExtras;
import br.com.rba.amaro.model.Product;
import br.com.rba.amaro.service.Service;

public class ProductDetailsActivity extends AppCompatActivity {

    private NetworkImageView productDetailsImage;
    private ImageView saleTag;
    private TextView productDetailsName;
    private TextView productDetailsPrice;
    private TextView productDetailsSalePrice;
    private TextView productDetailsSizes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.details_toolbar);
        setSupportActionBar(toolbar);

        Product product = null;

        if (getIntent().hasExtra(ProductExtras.TAG_PRODUCT)) {
            product = getIntent().getParcelableExtra(ProductExtras.TAG_PRODUCT);
        }

        productDetailsImage = findViewById(R.id.details_image);
        saleTag = findViewById(R.id.sale_tag);
        productDetailsName = findViewById(R.id.details_name);
        productDetailsPrice = findViewById(R.id.details_price);
        productDetailsSalePrice = findViewById(R.id.details_sales_price);
        productDetailsSizes = findViewById(R.id.detail_available_sizes);

        showProduct(product);
    }

    private void showProduct(Product product) {
        ImageLoader mImageLoader = Service.getInstance().getImageLoader();

        StringBuilder sizes = new StringBuilder();

        if (product != null) {

            productDetailsImage.setImageUrl(product.getImage(), mImageLoader);
            productDetailsName.setText(product.getName());

            if ((productDetailsPrice.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0) {
                productDetailsPrice.setPaintFlags(productDetailsPrice.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            }

            productDetailsPrice.setText(product.getRegularPrice());

            if (product.isOnSale()) {
                productDetailsPrice.setPaintFlags(productDetailsPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                productDetailsSalePrice.setText(product.getActualPrice());
                productDetailsSalePrice.setVisibility(View.VISIBLE);
                saleTag.setVisibility(View.VISIBLE);
                productDetailsPrice.setTypeface(Typeface.DEFAULT);
            } else {
                saleTag.setVisibility(View.GONE);
                productDetailsSalePrice.setVisibility(View.GONE);
            }


            for (int i = 0; i < product.getSizes().size(); i++) {
                sizes.append(product.getSizes().get(i));
                if (i != (product.getSizes().size() - 1)) {
                    sizes.append("  ");
                }
            }

            productDetailsSizes.setText(sizes);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
