package br.com.rba.amaro.service;

import android.app.Application;

public class ServiceApplication extends Application {

    private static final String PRODUCTS_URL = "http://www.mocky.io/v2/59b6a65a0f0000e90471257d";

    @Override
    public void onCreate() {
        super.onCreate();
        Service.getInstance().init(this);
    }

    public static String getProductsUrl() {
        return PRODUCTS_URL;
    }
}
