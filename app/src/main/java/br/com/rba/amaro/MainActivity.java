package br.com.rba.amaro;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.rba.amaro.adapter.CustomSpinnerAdapter;
import br.com.rba.amaro.adapter.ProductsAdapter;
import br.com.rba.amaro.commons.ProductExtras;
import br.com.rba.amaro.model.Product;
import br.com.rba.amaro.service.Service;
import br.com.rba.amaro.service.ServiceRequestListener;
import br.com.rba.amaro.utils.RecyclerItemClickListener;
import br.com.rba.amaro.utils.Utils;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ProductsAdapter adapter;
    private List<Product> productList;
    private List<Product> onSaleProductList;

    private boolean onSaleFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout = findViewById(R.id.main_content);
        progressBar = findViewById(R.id.progressbar);

        productList = new ArrayList<>();
        onSaleProductList = new ArrayList<>();

        initCollapsingToolbar();
        initFilterSpinner();
        initRecycleView();
        loadProducts();
        initGlide();
    }

    private void initFilterSpinner() {
        Spinner filterSpinner = findViewById(R.id.spn_filter);
        filterSpinner.setOnItemSelectedListener(this);

        List<String> filters = new ArrayList<>();
        filters.add(getResources().getString(R.string.all_items));
        filters.add(getResources().getString(R.string.on_sale_items));

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), filters);
        filterSpinner.setAdapter(customSpinnerAdapter);
    }

    private void initRecycleView() {

        adapter = new ProductsAdapter(this, productList);
        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        callProductDetailsActivity(view, onSaleFilter);
                    }
                }));
    }

    private void initGlide() {
        try {
            Glide.with(this).load(R.drawable.cover).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNoInternetSnackBar() {
        progressBar.setVisibility(View.GONE);
        final Snackbar noInternetSnackBar = Snackbar.make(coordinatorLayout, getString(R.string.no_internet_connection_text), Snackbar.LENGTH_INDEFINITE);
        noInternetSnackBar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noInternetSnackBar.dismiss();
                progressBar.setVisibility(View.VISIBLE);
                loadProducts();
            }
        });
        noInternetSnackBar.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                onSaleFilter = false;
                adapter.update(productList);
                adapter.notifyDataSetChanged();
                break;
            case 1:
                onSaleFilter = true;
                adapter.update(onSaleProductList);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        private GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void loadProducts() {
        if (Utils.isOnline(getApplicationContext())) {
            Service.getProducts(new ServiceRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            JSONArray products = new JSONArray(response.getString(ProductExtras.TAG_PRODUCTS));

                            for (int i = 0; i < products.length(); i++) {
                                addToProductList(products.getJSONObject(i));
                            }

                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(String error) {
                    showNoInternetSnackBar();
                }
            });
        } else {
            showNoInternetSnackBar();
        }
    }

    private void addToProductList(JSONObject productJson) throws JSONException {
        if (productJson != null) {

            Product product = new Product();

            if (!TextUtils.isEmpty(productJson.getString(ProductExtras.TAG_NAME))) {
                product.setName(productJson.getString(ProductExtras.TAG_NAME));
            }

            if (!TextUtils.isEmpty(productJson.getString(ProductExtras.TAG_ACTUAL_PRICE))) {
                product.setActualPrice(productJson.getString(ProductExtras.TAG_ACTUAL_PRICE));
            }

            if (!TextUtils.isEmpty(productJson.getString(ProductExtras.TAG_REGULAR_PRICE))) {
                product.setRegularPrice(productJson.getString(ProductExtras.TAG_REGULAR_PRICE));
            }

            if (!TextUtils.isEmpty(productJson.getString(ProductExtras.TAG_IMAGE))) {
                product.setImage(productJson.getString(ProductExtras.TAG_IMAGE));
            }

            product.setOnSale(productJson.getBoolean(ProductExtras.TAG_ON_SALE));

            if (productJson.getJSONArray(ProductExtras.TAG_SIZES) != null &&
                    productJson.getJSONArray(ProductExtras.TAG_SIZES).length() > 0) {
                ArrayList<String> availableSizes = addAvailableSizes(productJson.getJSONArray(ProductExtras.TAG_SIZES));
                product.setSizes(availableSizes);
            }

            if (product.isOnSale()) {
                onSaleProductList.add(product);
            }

            productList.add(product);
        }
    }

    private ArrayList<String> addAvailableSizes(JSONArray availableSizesJson) throws JSONException {

        ArrayList<String> availableSizes = new ArrayList<>();

        for (int i = 0; i < availableSizesJson.length(); i++) {

            JSONObject sizeObj = availableSizesJson.getJSONObject(i);

            if (sizeObj.getBoolean(ProductExtras.TAG_AVAILABLE) &&
                    (!TextUtils.isEmpty(sizeObj.getString(ProductExtras.TAG_SIZE)))) {
                availableSizes.add(sizeObj.getString(ProductExtras.TAG_SIZE));
            }
        }

        return availableSizes;
    }

    private void callProductDetailsActivity(View view, boolean onSale) {
        int itemPosition = recyclerView.getChildLayoutPosition(view);
        Product product;

        if (onSale) {
            product = onSaleProductList.get(itemPosition);
        } else {
            product = productList.get(itemPosition);
        }

        Intent it = new Intent(this, ProductDetailsActivity.class);
        it.putExtra(ProductExtras.TAG_PRODUCT, product);
        startActivity(it);
    }

}
