package br.com.rba.amaro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.rba.amaro.R;

public class CustomSpinnerAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflter;
    private List<String> itemsList;

    public CustomSpinnerAdapter(Context applicationContext, List<String> itemsList) {
        this.context = applicationContext;
        this.itemsList = itemsList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int i) {
        return 0;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView item = view.findViewById(R.id.spn_item);
        item.setText(itemsList.get(i));
        return view;
    }


}