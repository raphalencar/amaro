package br.com.rba.amaro.service;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class Service {

    private static RequestQueue mRequestQueue;
    private static Service mInstance;
    private static Context mApplicationContext;
    private ImageLoader mImageLoader;

    private Service() {
        super();
    }

    public static Service getInstance() {
        if (mInstance == null) {
            mInstance = new Service();
        }
        return mInstance;
    }

    public void init(Application application) {
        if (mApplicationContext == null) {
            mApplicationContext = application.getApplicationContext();
            mRequestQueue = Volley.newRequestQueue(mApplicationContext);
            mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

                public void putBitmap(String url, Bitmap bitmap) {
                    mCache.put(url, bitmap);
                }

                public Bitmap getBitmap(String url) {
                    return mCache.get(url);
                }
            });
        }
    }

    public static void getProducts(final ServiceRequestListener listener) {
        JsonObjectRequest request = new JsonObjectRequest
                (Request.Method.GET,
                        getProductsUrl(),
                        null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                listener.onResponse(response);
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.getMessage());
                    }
                });

        mRequestQueue.add(request);
    }

    private static String getProductsUrl() {
        return ServiceApplication.getProductsUrl();
    }

    public ImageLoader getImageLoader() {
        return this.mImageLoader;
    }
}
